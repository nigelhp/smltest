structure Test =
struct
  datatype expectation = Pass
                       | Fail of string

  type test_case = unit -> expectation
                               
  datatype outcome = Success
                   | Failure of string
                   | Error of exn
                               
  fun eval (test : test_case) : outcome =
      (case test () of
           Pass        => Success
         | Fail reason => Failure reason)
      handle err => Error err
end
