use "../main/Test.sml";

structure Assert =
struct
  fun that (test : Test.test_case) : unit =
      case test () of
          Test.Pass        => ()
        | Test.Fail reason => raise General.Fail reason
end;

let
  fun outcomeIsSuccessWhenTestCasePasses () : Test.expectation =
      let
        fun aPassingTestCase () : Test.expectation = Test.Pass
      in
        case Test.eval aPassingTestCase of
            Test.Success   => Test.Pass
          | Test.Failure _ => Test.Fail "Expected [Success] but was [Failure]"
          | Test.Error _   => Test.Fail "Expected [Success] but was [Error]"
      end
          
  fun outcomeIsFailureWhenTestCaseFails () : Test.expectation =
      let
        val reason = "Expected 10, but these go to 11"
        fun aFailingTestCase () : Test.expectation = Test.Fail reason
        fun unexpectedReasonOf (value : string) : string =
            "Expected Failure reason of [" ^ reason ^ "] but was [" ^
            value ^ "]"
      in
        case Test.eval aFailingTestCase of
            Test.Success             => Test.Fail "Expected [Failure] but was [Success]"
          | Test.Failure description => if description = reason
                                        then Test.Pass
                                        else Test.Fail (unexpectedReasonOf description)
          | Test.Error _             => Test.Fail "Expected [Failure] but was [Error]" 
      end

  fun outcomeIsErrorWhenTestCaseRaisesAnException () : Test.expectation =
      let
        val cause = Empty
        fun anExceptionRaisingTestCase () : Test.expectation = raise cause
        fun unexpectedCauseOf (value : exn) : string =
            "Expected Error cause of [" ^ (exnName cause) ^ "] but was [" ^
            (exnName value) ^ "]"
      in
        case Test.eval anExceptionRaisingTestCase of
            Test.Success      => Test.Fail "Expected [Error] but was [Success]"
          | Test.Failure _    => Test.Fail "Expected [Error] but was [Failure]"
          | Test.Error raised => case raised of
                                     Empty => Test.Pass
                                   | _     => Test.Fail (unexpectedCauseOf raised)
      end
in
  Assert.that outcomeIsSuccessWhenTestCasePasses;
  Assert.that outcomeIsFailureWhenTestCaseFails;
  Assert.that outcomeIsErrorWhenTestCaseRaisesAnException
end;
