## SML Test

In an attempt to distill the capabilities of a unit test library to their
absoulte core, the following requirements stand out:

* **_define_** a test case
* **_evaluate_** a test case
* **_report_** the outcome of evaluating a test case

All three capabilities must be present for the library to have utility, but
there seems to be benefit in isolating these characteristics.  For example,
there is clearly scope to report on the outcome of evaluating a test case in
many different ways.

On the concept of definition, we focus on the abstract rather than the specific
representation.  While matchers can greatly improve the readability of test case
expectations for example, the details of how a client developer chooses to
express expectations should be left open and should not be dictated by this
library.

In addition, even the smallest of projects will quickly discover the need for
abstractions that operate at a higher level than that of an individual test.  In
particular, we typically require a means of **_grouping_** test cases so that:

* they can be executed and reported upon in aggregate
* common setup and tear down behaviour can be abstracted, and applied to all
  members of a group


### A TODO List

[Beck's](#Beck) _'First Steps to xUnit'_ defines an initial TODO list of:
* invoke test method
* invoke setUp first
* invoke tearDown afterward
* invoke tearDown even if the test method fails
* run multiple tests
* report collected results

It is time to start with a test ...


### Invoke Test Method

As we commence development we immediately encounter a bootstrap problem - we
want to write tests to help us drive the development of a test framework without
yet having a test framework in which to write tests!

[Beck](#Beck) commences with a _'proto-test'_, a test that can be manually
verified via `print` statements:

> Here's the strategy for our bootstrap test: We will create a test case that
> contains a flag.  Before the test method is run, the flag should be false.
> The test method will set the flag.  After the test method is run, the flag
> should be true.

_([Beck](#Beck), p92)_


This implies an object-oriented approach, in keeping with the dominant paradigm
of the time.  At this very first step the implementation introduces mutable
state and side-effects.  It is notable that the test **method** (as opposed to
function) does not have a return value <sup>[1](#1)</sup> and that `print`
statements are required to observe the outcome.

For our purposes, a test case can be considered a 'black box' that evaluates
behaviour against some expectations, and then signals whether or not the
expectations were satisfied. 

In functional programming, our 'black box' and unit of behaviour is the
function.  We limit side-effects, and signal outcomes via a function's return
value <sup>[2](#2)</sup>.  Because functions are themselves values we can pass a
test case represented by a function to our evaluator, and have the return value
of the evaluation indicate whether or not the expectations were satisfied.
Handily, because we have a REPL, we do not require `print` statements to view
that outcome.

Our version of a bootstrap test is therefore:

    let
      fun passingTestCase () : bool = true
    in
      Test.eval passingTestCase
    end;
    
    let
      fun failingTestCase () : bool = false
    in
      Test.eval failingTestCase
    end;

and our implementation is simply:

    structure Test =
    struct
      type test_case = unit -> bool

      fun eval (test : test_case) : bool = test ()
    end

which yields the expected output at the REPL:

    val it = true: bool
    val it = false: bool


##### Adding A Failure Reason

Our `eval` function can now signal that a test has failed, but it provides no
clue as to _why_ it failed.  Some description of the actual outcome in
comparison with the expected outcome would greatly assist us in identifying the
cause of the issue.

It is evident that `bool` is too limited a return type, as a value of `false`
lacks the ability to convey any additional information.  We can resolve this by
defining a custom datatype; a sum type that represents that a test outcome is
either a `Pass` or a `Fail`, where a Fail can only be constructed with a
description of why the test failed.

This gives us a revised bootstrap test of:

    let
      fun passingTestCase () : Test.outcome = Test.Pass
    in
      Test.eval passingTestCase
    end;
    
    let
      fun failingTestCase () : Test.outcome = Test.Fail "Expected 10, but these go to 11"
    in
      Test.eval failingTestCase
    end;

a revised implementation:

    structure Test =
    struct
      datatype outcome = Pass
                       | Fail of string
                               
      type test_case = unit -> outcome
                             
      fun eval (test : test_case) : outcome = test ()
    end

and more meaningful output at the REPL of:

    val it = Pass: Test.outcome
    val it = Fail "Expected 10, but these go to 11": Test.outcome


### Beyond Bootstrap

We now have the ability to distinguish a failing from a passing test, and can
capture the reason for any failure.  Can we commence writing tests in this
fledgling framework?

Our simplest test is that the outcome of evaluating a passing test case is
itself a pass. 
    
    fun outcomeIsPassWhenTestCasePasses () : Test.outcome =
      let
        fun aPassingTestCase () : Test.outcome = Test.Pass
      in
        case Test.eval aPassingTestCase of
            Test.Pass   => Test.Pass
          | Test.Fail _ => Test.Fail "Expected [Pass] but was [Fail]"
      end;

Our expectation for the scenario when the test case fails can also be expressed,
but this requires a little more effort to ensure that the underlying failure
reason is retained.

    fun outcomeIsFailWhenTestCaseFails () : Test.outcome =
      let
        val reason = "Expected 10, but these go to 11"
        fun aFailingTestCase () : Test.outcome = Test.Fail reason
      in
        case Test.eval aFailingTestCase of
            Test.Pass             => Test.Fail "Expected [Fail] but was [Pass]"
          | Test.Fail description => if description = reason
                                     then Test.Pass
                                     else Test.Fail ("Expected Fail reason of [" ^ reason ^
                                                     "] but was [" ^ description ^ "]")
      end;

If we evaluate these tests with our current implementation of `eval`, they pass
as expected:

    val it = Pass: Test.outcome
    val it = Pass: Test.outcome

Unfortunately, it is all too easy for this approach to lull us into a false
sense of security.  The function under test here is `eval`, and so it is not
safe for us to use `eval` itself to evaluate this test.

As an illustration, consider what would happen if `eval` was mistakenly (or
maliciously) implemented as follows:

    fun eval (test : test_case) : outcome = Pass;

Our tests would continue to indicate that they pass, when in fact none of the
defined behaviour would actually be evaluated.  If we are to build upon the
foundation of `eval`, we need to be confident that any regressions in its
implementation will be caught by tests.

We therefore introduce an alternative mechanism to evaluate the tests for
`eval`.  In order to avoid having to manually inspect REPL output to confirm
that these are passing, we simply raise an exception on failure, which
(when unhandled) will cause the REPL to exit.  A drastic measure perhaps, but
one that allows us to move beyond a bootstrap test, and build upon `eval` with
confidence.

    structure Assert =
    struct
      fun that (test : Test.test_case) : unit =
          case test () of
              Test.Pass        => ()
            | Test.Fail reason => raise General.Fail reason
    end;

We can now execute our existing test cases as follows:

    Assert.that outcomeIsPassWhenTestCasePasses;
    Assert.that outcomeIsFailWhenTestCaseFails

No exceptions are raised for the current implementation of `eval`.  Moreover, we
are now protected from the poor implementation that always returns `Pass`:

    Exception- Fail "Expected [Fail] but was [Pass]" raised

along with a poor implementation that always returns `Fail`:

    Exception- Fail "Expected [Pass] but was [Fail]" raised

and finally an implementation that does not pass through the failure reason:

    Exception- Fail "Expected Fail reason of [Expected 10, but these go to 11] but was [none more black]" raised

Note that we are relying on `Assert.that` to give us confidence in the
implementation of `eval`, but do not have tests to give us confidence in the
implementation of `Assert.that`.  This is not ideal, but such tests would
encounter the same dependency issue encountered above - they should not depend
on `eval`, and cannot depend on `Assert.that`.  We would require yet another
layer, but how would we then test that layer?

Pragmatically, if uncomfortably, we can move on.  The implementation of
`Assert.that` reads well, and seems simple enough to not be hiding any major
flaws.  We have confidence from the experiments detailed above that the
implementation is sufficient for our current purposes, and we do not envisage
that we will need to change this much going forwards.


### Side Effects

We have so far considered a test case to be a pure function of type `unit ->
outcome`.  The reality is a little more complicated, as any function in SML can
raise an exception rather than return a value.  Such behaviour is not made
explicit in a function's type, and effectively means that any function may be
partial rather than total.

If we evaluate such a test case at present, the exception is propagated out of
`eval`, such that `eval` itself is a partial function.

    fun anExceptionRaisingTestCase () : Test.outcome = raise Empty;
    Test.eval anExceptionRaisingTestCase;

    Exception- Empty raised

Instead, we want to handle such exceptions, and report them along with the
passes and failures.  We need `eval` to be a total function, and in order to
achieve that, our types must reflect the distinction between the test
expectation and test outcome.

A test case encapsulates an expectation which may or may not be satisfied:

    datatype expectation = Pass
                         | Fail of string

    type test_case = unit -> expectation

The evaluation of a test case may receive a result, or encounter an exception
(which we describe as an `Error` outcome):

    datatype outcome = Success
                     | Failure of string
                     | Error of exn

It is trivial to refactor our existing tests to these new types, and to define a
new test for the scenario in which an exception is raised:

    fun outcomeIsErrorWhenTestCaseRaisesAnException () : Test.expectation =
        let
          val cause = Empty
          fun anExceptionRaisingTestCase () : Test.expectation = raise cause
          fun unexpectedCauseOf (value : exn) : string =
              "Expected Error cause of [" ^ (exnName cause) ^ "] but was [" ^
              (exnName value) ^ "]"
        in
          case Test.eval anExceptionRaisingTestCase of
              Test.Success      => Test.Fail "Expected [Error] but was [Success]"
            | Test.Failure _    => Test.Fail "Expected [Error] but was [Failure]"
            | Test.Error raised => case raised of
                                       Empty => Test.Pass
                                     | _     => Test.Fail (unexpectedCauseOf raised)
        end

Finally, our implementation requires a suitable exception handler:

    fun eval (test : test_case) : outcome =
      (case test () of
           Pass        => Success
         | Fail reason => Failure reason)
      handle err => Error err
      
That completes the first item on our todo list - the ability to invoke a test
method.  This implementation has been bootstrapped via assertions, so that we
can now write subsequent tests within our own fledgling framework.  As we try to
do so, we will quickly recognise the need to conveniently be able to run
multiple tests.  This is the next item we will tackle from our todo list.


### Footnotes
<a name="1">1</a>: The xUnit framework typically defines a testCase as a void /
no return value method.  An exception is then thrown to signal to the framework
that an expectation has failed to be met, for example [JUnit](#JUnit) throws
`java.lang.AssertionError`.

<a name="2">2</a>: In practice, we will also have to handle the scenario where a
runtime exception is thrown during the evaluation of a test case.  e.g. rather
than calculating a value that is incorrect, our implementation code may
encounter an error and fail to return a value at all.


### References
* <a name="Beck">Beck</a>, Kent.  Test-Driven Development: By Example,
  Addison-Wesley, 2003.
* <a name="JUnit">[JUnit](https://junit.org)</a>
